<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    </head>
    <body>
        <h1>Observer</h1>
        <ul>
            <li>observeur 1 : {{ $observer1->getValue() }}</li>
            <li>observeur 2 : {{ $observer2->getValue() }}</li>
        </ul>
    </body>
</html>
