<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    </head>
    <body>
        <h1>Observer</h1>
        <ul>
            <li>Opel : {{ $opel }}</li>
            <li>Nissan : {{ $nissan }}</li>
            <li>Renault : {{ $renault }}</li>
        </ul>
        <p>Don't understand why is it not working ? I think the getCATotal function is good :P</p>
    </body>
</html>
