<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    </head>
    <body>
        <h1>Chain Of Responsability</h1>
        <ul>
            <li>a - 1 : {{ $a }}</li>
            <li>b - "String" : {{ $b }}</li>
            <li>c - 0.1 : {{ $c }}</li>
        </ul>
    </body>
</html>
