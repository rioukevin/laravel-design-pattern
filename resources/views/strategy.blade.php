<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    </head>
    <body>
        <h1>Strategy</h1>
        <ul>
            <li>Voiture francaises : {{ $voitureFR->getTVA() }}% de TVA sur une {{ $voitureFR->getNom() }}</li>
            <li>voiture non-francaise : {{ $voitureAutre->getTVA() }}% de TVA sur une {{ $voitureAutre->getNom() }}</li>
        </ul>
    </body>
</html>
