<?php

namespace App;

use App\Subject;

class Observer
{
    private $value = "true";

    public function __CONSTRUCT(){}
    
    public function update()
    {
        if ($this->value == "true")
        {
            $this->value = "false";
        }
        else
        {
            $this->value = "true";
        }
    }

    public function getValue()
    {
        return $this->value;
    }
}
