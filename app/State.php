<?php

namespace App;

use App\State0;
use App\State1;
use App\State2;

class State
{
    private $etat = 0;
    private $state;

    public function __construct()
    {

    }

    public function operation($monInt)
    {
        if ($this->etat == 0)
        {
            $state = new State0();
            $this->etat = 1;
            return $state->operation($monInt);
        }
        else if ($this->etat == 1)
        {
            $state = new State1();
            $this->etat = 2;
            return $state->operation($monInt);
        }
        else
        {
            $state = new State2();
            $this->etat = 0;
            return $state->operation($monInt);
        }
    }
}
