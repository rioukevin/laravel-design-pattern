<?php

namespace App;

use App\VoitureFR;
use App\VoitureAutre;

class FactureLine
{
    private $voiture;

    public function __CONSTRUCT(string $voiture)
    {
        if ($voiture == "Renault")
        {
            $this->voiture = new VoitureFR("$voiture");
        }
        else
        {
            $this->voiture = new VoitureAutre("$voiture");
        }
    }

    public function getTVA()
    {
        return $this->voiture->getTVA();
    }

    public function getNom()
    {
        return $this->voiture->getNom();
    }
}
