<?php

namespace App;

use App\Concession;
use App\Facturation;
use App\Usine;

class Commande
{
    private $usine;
    private $concession;
    private $facturation;

    public function __CONSTRUCT()
    {
        $this->usine = new Usine();
        $this->facturation = new Facturation();
        $this->concession = new Concession();
    }

    public function commander(string $voiture)
    {
        $this->usine->fabriquer($voiture);
        $this->concession->récupérerVoiture($this->usine->getVoiture());
        return "Usine : ".$this->usine->getVoiture()." Concession : ".$this->concession->getNomConcession()." Voiture à la concession : ".$this->concession->getVoiture()." Facturation : ".$this->facturation->getPrix();
    }
}
