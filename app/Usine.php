<?php

namespace App;

class Usine
{
    private $voiture = "";

    public function __CONSTRUCT()
    {

    }

    public function fabriquer(string $type)
    {
        $this->voiture = $type;
    }

    public function getVoiture()
    {
        return $this->voiture;
    }
}
