<?php

namespace App;

class Factory
{
    public function __CONSTRUCT()
    {}

    public function createVoiture(string $voiture)
    {
        switch($voiture)
        {
            case "Opel":
                return new OpelClasse();
                break;
            case "Renaud":
                return new RenaudClasse();
                break;
            default:
                return false;
                break;
        }

    }
}
