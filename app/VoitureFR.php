<?php

namespace App;

class VoitureFR
{
    private $nom;
    private $TVA = 20;

    public function __CONSTRUCT(string $nom)
    {
        $this->nom = $nom;
    }

    public function getTVA()
    {
        return $this->TVA;
    }

    public function getNom()
    {
        return $this->nom;
    }
}
