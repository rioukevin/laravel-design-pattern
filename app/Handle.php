<?php

namespace App;

use App\H1;
use App\H2;
use App\H3;

class Handle
{
    public function __CONSTRUCT()
    {

    }

    public function handle(string $type)
    {
        switch($type)
        {
            case "integer":
                $handler = new H1();
                return $handler->handle();
                break;
            case "string":
                $handler = new H2();
                return $handler->handle();
                break;
            default:
                $handler = new H3();
                return $handler->handle();
                break;
        }
    }
}
