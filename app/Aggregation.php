<?php

namespace App;

use App\Iterateur;

class Aggregation
{
    public function __CONSTRUCT(){}

    public function createIterateur(array $liste)
    {
        return new Iterateur($liste);
    }
}
