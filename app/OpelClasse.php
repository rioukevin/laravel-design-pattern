<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpelClasse extends Model
{
    public function __CONSTRUCT()
    {}

    public function get_class()
    {
        return __CLASS__;
    }
}
