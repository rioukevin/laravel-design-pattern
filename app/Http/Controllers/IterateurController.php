<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aggregation;

class IterateurController extends Controller
{
    function go()
    {
        $liste = ["Pomme", "Poire", "Banane"];

        $aggregation = new Aggregation();

        $iterateur = $aggregation->createIterateur($liste);

        return view('iterateur', ['iterateur'=>$iterateur]);
    }
}
