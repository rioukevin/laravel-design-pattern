<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Singleton;

class SingletonController extends Controller
{
    function go()
    {
        $a = Singleton::getInstance();

        $a->incremente();

        $b = Singleton::getInstance();

        return view('singleton', ['a'=>$a->getCount(), 'b'=>$b->getCount()]);
    }
}
