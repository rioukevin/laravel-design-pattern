<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\FactureLine;

class StrategyController extends Controller
{
    public function go()
    {
        $voitureFR = new FactureLine("Renault");
        $voitureAutre = new FactureLine("Ferrari");

        return view('strategy', ['voitureFR' => $voitureFR, 'voitureAutre' => $voitureAutre]);
    }
}
