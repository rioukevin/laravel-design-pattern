<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Observer;
use App\Subject;

class ObserverController extends Controller
{
    function go()
    {
        $subject = new Subject();

        $observer1 = new Observer();
        $observer2 = new Observer();

        $subject->register($observer1);
        $subject->register($observer2);

        return view('observer', ['subject'=>$subject, 'observer1' => $observer1, 'observer2' => $observer2]);
    }
}
