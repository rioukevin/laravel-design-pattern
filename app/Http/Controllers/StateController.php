<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\State;

class StateController extends Controller
{
    function go()
    {
        $monInt = 0;

        $state = new State();

        $a = $state->operation($monInt);

        $b = $state->operation($monInt);

        $c = $state->operation($monInt);

        return view('state', ['a'=>$a, 'b'=>$b, 'c'=>$c]);
    }
}
