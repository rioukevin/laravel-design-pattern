<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Commande;

class ControllerFacade extends Controller
{
    public function go()
    {
        $commande = new Commande();

        $maCommande = $commande->commander("Opel");

        return view('facade', ['commande' => $maCommande]);
    }
}
