<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Handle;

class CorController extends Controller
{
    function go()
    {
        $a = 0;
        $b = "oui";
        $c = 0.1;

        $handler = new Handle();

        return view('cor', ['a'=>$handler->handle(gettype($a)), 'b'=>$handler->handle(gettype($b)), 'c'=>$handler->handle(gettype($c))]);
    }
}
