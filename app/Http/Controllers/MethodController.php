<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TemplateVoiture1;
use App\TemplateVoiture2;

//This is the template conroller, I miss on the name ;)

class MethodController extends Controller
{
    public function go()
    {
        $voiture1 = new TemplateVoiture1();
        $voiture2 = new TemplateVoiture2();

        return view('template', ['voiture1' => $voiture1->templateFunction(), 'voiture2' => $voiture2->templateFunction()]);
    }
}
