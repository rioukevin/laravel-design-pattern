<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Factory;

class FactoryController extends Controller
{
    public function go()
    {
        $factory = new Factory();

        $voitureOpel = $factory->createVoiture("Opel");
        $voitureRenaud = $factory->createVoiture("Renaud");
        $voitureIncorrect = $factory->createVoiture("BMW");

        return view('factory', ['voitureOpel' => $voitureOpel->get_class(), 'voitureRenaud' => $voitureRenaud->get_class()/*, 'voitureIncorrect' => $voitureIncorrect->get_class()*/]);
    }
}
