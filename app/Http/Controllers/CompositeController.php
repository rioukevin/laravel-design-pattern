<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Filiale;

class CompositeController extends Controller
{
    public function go()
    {
        $renault = new Filiale(5);
        $opel = new Filiale(2);
        $nissan = new Filiale(2);
        $mitsubishi = new Filiale(1);
        $subaru = new Filiale(1);
        $nissan->addFiliale($mitsubishi);
        $nissan->addFiliale($subaru);
        $renault->addFiliale($nissan);
        $renault->addFiliale($opel);

        return view('composite', ['renault' => $renault->getCATotal(),'opel' => $opel->getCATotal(),'nissan' => $nissan->getCATotal()]);
    }
}
