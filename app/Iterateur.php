<?php

namespace App;

class Iterateur
{
    private $liste;
    private $index = 0;

    public function __CONSTRUCT(array $liste)
    {
        $this->liste = $liste;
    }

    public function hasNext()
    {
        if ($this->index < count($this->liste))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function next()
    {
        if ($this->hasNext())
        {
            $res = $this->liste[$this->index];
            $this->index++;
            return $res;
        }
    }
}
