<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RenaudClasse extends Model
{
    public function __CONSTRUCT()
    {}

    public function get_class()
    {
        return __CLASS__;
    }
}
