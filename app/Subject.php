<?php

namespace App;

use App\Observer;

class Subject
{
    private $observers = [];

    public function __CONSTRUCT(){}

    public function register(Observer $observer)
    {
        array_push($this->observers, $observer);
    }

    public function unregister(Observer $observer)
    {
        array_pull($this->observers, $observer);
    }

    public function notifyAll()
    {
        foreach ($this->observers as $observer)
        {
            $observer->update();
        }
    }
}
