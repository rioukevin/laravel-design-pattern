<?php

namespace App;

class Concession
{
    private $voiture = "";
    private $concession = "Lannion";

    public function __CONSTRUCT()
    {

    }

    public function récupérerVoiture(string $voiture)
    {
        $this->voiture = $voiture;
    }

    public function getVoiture()
    {
        return $this->voiture;
    }

    public function getNomConcession()
    {
        return $this->concession;
    }
}
