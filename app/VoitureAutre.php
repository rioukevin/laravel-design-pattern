<?php

namespace App;

class VoitureAutre
{
    private $nom;
    private $TVA = 13;

    public function __CONSTRUCT(string $nom)
    {
        $this->nom = $nom;
    }

    public function getTVA()
    {
        return $this->TVA;
    }

    public function getNom()
    {
        return $this->nom;
    }
}
