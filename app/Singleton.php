<?php

namespace App;

class Singleton
{
    private static $instance = null;
    private $counter;

    private function __construct()
    {
        $this->counter = 0;
    }

    public static function getInstance()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new Singleton();
        }
        return self::$instance;
    }

    public function incremente()
    {
        $this->counter++;
    }

    public function getCount()
    {
        return $this->counter;
    }
}
