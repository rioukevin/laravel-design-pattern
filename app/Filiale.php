<?php

namespace App;

use App\Composite;

class Filiale implements Composite
{
    private $liste_filiale = [];
    private $CA;
    public function __construct($CAInit = 0){
        $this->CA=$CAInit;
    }

    public function getCATotal(){
        $res = 0;
        if(count($this->liste_filiale) > 0){
            //Just see for the under level filiales CA and increment it
            foreach($this->liste_filiale as $elem){
                $res += $elem->getCATotal();
            }
        }else{
            $res = $this->CA;
        }

        return $res;
    }

    public function addFiliale($filliale){
        $this->liste_filiale[] = $filliale;
    }
}


