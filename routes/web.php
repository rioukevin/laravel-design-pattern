<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/singleton', 'SingletonController@go');

Route::get('/facade', 'ControllerFacade@go');

Route::get('/factory', 'FactoryController@go');

Route::get('/iterateur', 'IterateurController@go');

Route::get('/observer', 'ObserverController@go');

Route::get('/strategy', 'StrategyController@go');

Route::get('/templatemethod', 'MethodController@go');

Route::get('/command', 'CommandController@go');

Route::get('/cor', 'CorController@go');

Route::get('/state', 'StateController@go');

Route::get('/composite', 'CompositeController@go');
